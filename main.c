#include <CoreFoundation/CoreFoundation.h>
#import "AmbientInterface.h"

int main (int argc, const char * argv[]) {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    AmbientInterface* ai = [[AmbientInterface alloc] init];

    
    if(argc < 2){
        CFShow(CFSTR("./AmbientTest <request string>\n"));        
    }else {
        [ai sendRequest:[NSString stringWithCString:argv[1] encoding:NSASCIIStringEncoding]];
    }
    [ai release];
    
    [pool drain]; // This one might not strictly speaking be neccessary.
    //[pool release];

    return 0;
}



