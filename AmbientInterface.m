//
//  AmbientInterface.m
//  AmbientTest
//
//  Created by Eric Betts on 11/16/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import "AmbientInterface.h"


@implementation AmbientInterface


#pragma mark -
#pragma mark AmbientNotify Protocol
#pragma	mark -

@protocol AmbientNotify
- (oneway void) request:(NSString*)request;
@end 

#pragma mark -
#pragma mark sendRequest
#pragma	mark -

- (void) sendRequest:(NSString*) request {
    id proxy;
    NSConnection *connection;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidDie:) name:NSConnectionDidDieNotification object:connection];
    //NSLog(@"Starting request method");
    
    connection = [NSConnection connectionWithRegisteredName:@"org.ericbetts.ambientnotify" host:nil];
    
    if(connection){
        //NSLog(@"Obtained a connection %@", connection);
        proxy = [[connection rootProxy] retain];
        [proxy setProtocolForProxy:@protocol(AmbientNotify)];
        //NSLog(@"Retained the connection's rootProxy %@", proxy);
    
        if(proxy){
            [proxy request:request];
            NSLog(@"AmbientTest: request sent");

        }else{
            NSLog(@"AmbientTest: request not sent"); 
        }
    }
     
}        

@end
