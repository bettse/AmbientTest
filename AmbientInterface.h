//
//  AmbientInterface.h
//  AmbientTest
//
//  Created by Eric Betts on 11/16/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface AmbientInterface : NSObject {

}

-(void)sendRequest:(NSString *)request;

@end
